//
//  IBOperationManagerTest.m
//  IBFFNetwoking
//
//  Created by Mahmoud on 8/7/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "IBOperationManager.h"

@interface IBOperationManagerTest : XCTestCase

@property (nonatomic) IBOperationManager *opMan;

@end

@implementation IBOperationManagerTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.opMan = [IBOperationManager sharedInstance];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}


#pragma mark - helper methods

- (IBOperationManager *)createUniqueInstance {
    
    return [[IBOperationManager alloc] init];
    
}

#pragma mark - tests

- (void)testSingletonSharedInstanceCreated {
    
    XCTAssertNotNil([IBOperationManager sharedInstance]);
    
}

- (void)testSingletonUniqueInstanceCreated {
    
    XCTAssertNotNil([self createUniqueInstance]);
    
}

- (void)testSingletonReturnsSameSharedInstanceTwice {
    
    IBOperationManager *s1 = [IBOperationManager sharedInstance];
    XCTAssertEqual(s1,[IBOperationManager sharedInstance]);
    
}

- (void)testSingletonSharedInstanceSeparateFromUniqueInstance {
    
    IBOperationManager *s1 = [IBOperationManager sharedInstance];
    XCTAssertNotEqual(s1, [self createUniqueInstance]);
}

- (void)testSingletonReturnsSeparateUniqueInstances {
    
    IBOperationManager *s1 = [self createUniqueInstance];
    XCTAssertNotEqual(s1, [self createUniqueInstance]);
}

//- (void)testDoSomethingThatTakesSomeTime {
//
////    XCTestExpectation *completionExpectation = [self expectationWithDescription:@"Long method"];
//    
//    NSDictionary *params =@{@"categoryId": @"1"};
//    
//    [_opMan callWebServiceWithMethodName:POST andURL:@"http://kasb.azurewebsites.net/Api/GetProductByCatId" andParameters:params withSuccess:^(id response) {
//        
//        NSLog(@"test success");
//        
////        [completionExpectation fulfill];
//        
//    } withFailureBlock:^(NSError *error) {
////        XCTAssertEqualObjects(@"result", error, @"Result was not correct!");
//        NSLog(@"test success");
////        [completionExpectation fulfill];
//    }];
//    
//    
//    [self waitForExpectationsWithTimeout:70.0 handler:nil];
//}

@end
