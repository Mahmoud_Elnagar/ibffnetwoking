### IBFFNetwoking ###

===========================================================================
DESCRIPTION:

IBFFNetwoking is a Network Framework that you can use to call Post and Get Requests

And can also download image from URL.



Using the Application

To Use IBFFNetwoking :

1- Add IBFFNetwoking.framework file to your Project
2- import those files

#import <IBFFNetwoking/IBOperationManager.h>
#import <IBFFNetwoking/UIImageView+IBImageDownloader.h>

Call Post or Get request example :

NSDictionary *params =@{@"categoryId": @"1"};
    
    [[IBOperationManager sharedInstance] callWebServiceWithMethodName:POST andURL: @“your URL” andParameters: <your parameters as dictionary> withSuccess:^(id response) {
        


    } withFailureBlock:^(NSError *error) {
        
   }];
   

To download image :
    
    [_imgView setImageWithURL:@"http://clipart-library.com/data_images/165699.jpg" andPlaceHolder:[UIImage imageNamed:@"165705"]];