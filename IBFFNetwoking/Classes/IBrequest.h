//
//  IBrequest.h
//  IBFFNetwoking
//
//  Created by Mahmoud on 8/8/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IBOperationManager.h"

@interface IBrequest : NSObject

@property (strong, nonatomic) NSString * url;
@property (strong, nonatomic) NSDictionary * parameters;
@property IBMethodType method;

@property (copy) void (^sucsessBlock) (id);
@property (copy) void (^failureBlock) (NSError *);

@end
