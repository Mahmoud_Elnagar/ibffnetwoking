//
//  IBNetworkLayer.h
//  IBNetwoking
//
//  Created by Mahmoud on 8/6/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface IBNetworkLayer : NSObject

+ (void)postDataToWebServiceWithURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^) (NSError * error))failureBlock;


+ (void)getDataFromWebServiceWithURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^) (NSError * error))failureBlock;

@end
