//
//  IBOperationManager.m
//  IBNetwoking
//
//  Created by Mahmoud on 8/6/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "IBOperationManager.h"
#import "IBNetworkLayer.h"
#import "Reachability.h"
#import "IBrequest.h"


//Two static properties to know number of running requests
static NSMutableArray * runningThreads;
static int running;

@implementation IBOperationManager {
    Reachability * reachability;
}

#pragma mark - Initialization

+ (IBOperationManager *)sharedInstance
{
    
    //Initialize one instance as a singltone object
    static dispatch_once_t onceToken = 0;
    
    __strong static IBOperationManager * _sharedObject = nil;
    dispatch_once(&onceToken, ^{
        
        runningThreads = [NSMutableArray new];
        running = 0;
        _sharedObject = [[IBOperationManager alloc] init];
    });
    return _sharedObject;
}

//the only method that can be called for request

-(void)callWebServiceWithMethodName:(IBMethodType)method andURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^)(NSError *))failureBlock {
    
    
    //check reachabilty if not reachable it call failure block directly
    
    reachability = [Reachability reachabilityForInternetConnection];
    
    if (reachability.isReachable){
        
        
        //network reachable and add object to the array of requests
        
        IBrequest *req = [IBrequest new];
        req.method = method;
        req.parameters = params;
        req.url = url;
        [req setSucsessBlock:successBlock];
        [req setFailureBlock:failureBlock];
        
        [runningThreads addObject:req];
        
        //check next request to be excuted
        [self checkCount];
        
    }else {
        NSString *domain = @"com.ib.IBNetwoking.ConnectionError";
        NSString *desc = NSLocalizedString(@"Unable to Connect To Network", @"");
        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : desc };
        
        NSError *error = [NSError errorWithDomain:domain
                                             code: 101
                                         userInfo:userInfo];
        failureBlock(error);
    }
}


//check number of requests available and run next request
-(void) checkCount {
    
    if (runningThreads.count > 0) {
        if ([reachability isReachableViaWiFi]) {
            if (running <= 6) {
                running += 1;
                [self callService:[runningThreads objectAtIndex:0]];
                
            }
        }else {
            if (running <= 2) {
                running += 1;
                [self callService:[runningThreads objectAtIndex:0]];
                
            }
        }
    }
}

-(void) callService:(IBrequest *) request {
    
    if (request.method == POST)  {
        [IBNetworkLayer postDataToWebServiceWithURL:request.url andParameters:request.parameters withSuccess:^(id response) {
            
            request.sucsessBlock(response);
            running -= 1;
            [runningThreads removeObject:request];
            [self checkCount];
            
        } withFailureBlock:^(NSError *error) {
            request.failureBlock(error);
            running -= 1;
            [runningThreads removeObject:request];
            [self checkCount];
        }];
        
    }else if (request.method == GET) {
        
        [IBNetworkLayer getDataFromWebServiceWithURL:request.url andParameters:request.parameters withSuccess:^(id response) {
            
            request.sucsessBlock(response);
            running -= 1;
            [runningThreads removeObject:request];
            [self checkCount];
            
        } withFailureBlock:^(NSError *error) {
            
            request.failureBlock(error);
            running -= 1;
            [runningThreads removeObject:request];
            [self checkCount];
        }];
    }
}

@end
