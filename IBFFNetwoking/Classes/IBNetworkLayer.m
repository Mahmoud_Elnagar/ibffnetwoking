//
//  IBNetworkLayer.m
//  IBNetwoking
//
//  Created by Mahmoud on 8/6/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "IBNetworkLayer.h"

@implementation IBNetworkLayer


// call request with Get Method
+(void)getDataFromWebServiceWithURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^)(NSError * error))failureBlock {
    
    NSError *passedError;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *fullURL = [NSURL URLWithString: url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL: fullURL
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [urlRequest setHTTPMethod:@"GET"];
    if (params) {
        NSData *gettData = [NSJSONSerialization dataWithJSONObject: params options:0 error:&passedError];
        [urlRequest setHTTPBody:gettData];
    }
    
    NSURLSessionDataTask *gettDataTask = [session dataTaskWithRequest: urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error) {
            failureBlock(error);
        }else {
            
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if (error)
                failureBlock(error);
            else{
                successBlock(response);
            }
        }
    }];
    
    [gettDataTask resume];
}


// call request with Post Method
+(void)postDataToWebServiceWithURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^)(NSError * error))failureBlock {
    
    NSError *error;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURL *fullURL = [NSURL URLWithString: url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL: fullURL
                                                              cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                          timeoutInterval:60.0];
    
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [urlRequest setHTTPMethod:@"POST"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject: params options:0 error:&error];
    [urlRequest setHTTPBody:postData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest: urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        
        if (error) {
            failureBlock(error);
        }else {
            
            id response = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
            
            if (error)
                failureBlock(error);
            else{
                successBlock(response);
            }
            
        }
        
        
    }];
    
    [postDataTask resume];
}
@end
