//
//  UIImageView+IBImageDownloader.m
//  IBFFNetwoking
//
//  Created by Mahmoud on 8/8/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "UIImageView+IBImageDownloader.h"
#import "MBProgressHUD.h"

@implementation UIImageView (IBImageDownloader)

-(void)setImageWithURL:(NSString *)url andPlaceHolder:(UIImage *)image {
    
    if (image) {
        [self setImage:image];
    }
    
    [MBProgressHUD showHUDAddedTo:self animated:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               [MBProgressHUD hideHUDForView:self animated:YES];
                               if ( !error )
                               {
                                   UIImage *img = [[UIImage alloc] initWithData:data];
                                   [self setImage:img];
                               }
                           }];
    
    
}
@end
