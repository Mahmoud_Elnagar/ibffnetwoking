//
//  UIImageView+IBImageDownloader.h
//  IBFFNetwoking
//
//  Created by Mahmoud on 8/8/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIImageView (IBImageDownloader)

-(void)setImageWithURL:(NSString*) url andPlaceHolder:(UIImage*)image;

@end
