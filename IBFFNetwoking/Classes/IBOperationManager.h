//
//  IBOperationManager.h
//  IBNetwoking
//
//  Created by Mahmoud on 8/6/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBOperationManager : NSObject

//Enum Type for methods name
typedef enum {
    POST,
    GET
} IBMethodType;

+ (IBOperationManager *)sharedInstance;

- (void)callWebServiceWithMethodName:(IBMethodType)method andURL:(NSString *)url andParameters:(NSDictionary *)params withSuccess:(void (^)(id))successBlock withFailureBlock:(void (^) (NSError * error))failureBlock;

@end
